import dispatcher from 'dispatcher';
import store from 'store';

let items = {}
let idName = 'new-id-';
let idNum  = 1;


let _handleChange = () => {
	let storeData = store.getData();
};

let _add = (items, element) => {
	let id = element.getAttribute('data-id');

	if (!id) {
		id = idName + idNum;
		idNum++;
	}

	items[id] = {
		id: id,
		element: element
	}
};

let _handleMutate = () => {
    items = {};
    let elements = document.querySelectorAll('.className');
	for (let i = 0; i < elements.length; i++) {
		_add(items, elements[i]);
	}
};

let init = () => {
	_handleMutate();
	_handleChange();

	store.eventEmitter.subscribe(_handleChange);

	dispatcher.subscribe((e) => {
		if (e.type === 'mutate') {
			_handleMutate();
			_handleChange();
		}
	});
};

export default {
	init: init
}
