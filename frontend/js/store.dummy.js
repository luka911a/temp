import dispatcher from 'dispatcher';

let initialized = false;

let _handleEvent = (e) => {

};

let _init = () => dispatcher.subscribe(_handleEvent);

let eventEmitter = (() => {
	let _handlers = [];

	let dispatch = (event) => {
		for (let i = _handlers.length - 1; i >= 0; i--) {
			_handlers[i](event);
		}
	};
	let subscribe = (handler) => {
		_handlers.push(handler);
	};
	let unsubscribe = (handler) => {
		for (let i = 0; i <= _handlers.length - 1; i++) {
			if (_handlers[i] == handler) {
				_handlers.splice(i--, 1);
			}
		}
	};

	return {
		dispatch: dispatch,
		subscribe: subscribe,
		unsubscribe: unsubscribe
	}
})();

let getData = () => {

};

if (!initialized) {
	initialized = true;
	_init();
}

export default {
	eventEmitter: eventEmitter,
	getData: getData
}
