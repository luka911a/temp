
	let offset = function(elem) {
		function getOffsetSum(elem) {
			let top = 0, left = 0;
			while(elem) {
				top = top + parseInt(elem.offsetTop);
				left = left + parseInt(elem.offsetLeft);
				elem = elem.offsetParent;
			}

			return {top: top, left: left};
		}

		function getOffsetRect(elem) {
			let box = elem.getBoundingClientRect();

			let body = document.body;
			let docElem = document.documentElement;

			let scrollTop = window.pageYOffset  || docElem.scrollTop || body.scrollTop;
			let scrollLeft = window.pageXOffset || docElem.scrollLeft || body.scrollLeft;

			let clientTop = docElem.clientTop   || body.clientTop || 0;
			let clientLeft = docElem.clientLeft || body.clientLeft || 0;

			let top  = box.top  + scrollTop  - clientTop;
			let left = box.left + scrollLeft - clientLeft;

			return {
				top:  Math.round(top),
				left: Math.round(left)
			};
		}

		if (elem.getBoundingClientRect) {
			return getOffsetRect(elem);
		} else {
			return getOffsetSum(elem);
		}
	};

	let requestAnimFrame = (function(){
		return  window.requestAnimationFrame   ||
			window.webkitRequestAnimationFrame ||
			window.mozRequestAnimationFrame    ||
			window.oRequestAnimationFrame      ||
			window.msRequestAnimationFrame     ||
			function(callback, element){
				window.setTimeout(callback, 1000 / 60);
			}
	})();

	let Tween = function(from, to, duration, easing, func) {
		this.i = 0;
		this.duration = duration*60;
		this.current  = from;
		this.func     = func;
		this.to     = to;
		this.halt     = false;

		this.stop = function() {
			this.halt = true;
			this.i = 0;
			this.current = from;
		};

		this.animate = function() {
			let scope = this;
			this.halt = false;

			this.func(this.current);

			let loop = function() {
				let progress;
				let delta;
				let current;

				if (scope.halt) {
					return;
				}

				scope.i++;
				progress = scope.i/scope.duration;
				if (progress > 1) {
					progress = 1;
				}

				if (easing) {
					delta = easing(progress);
				} else {
					delta = progress;
				}

				scope.current = from + (to - from)*delta;

				scope.func(scope.current);


				if (Math.floor(scope.current - scope.to) === 0)  {
					return;
				}

				requestAnimFrame(loop);
			};

			loop();
		}
	};

	let ajax = {
		send: function(url, callback, method, data, sync) {
			let x = new XMLHttpRequest();
			x.open(method, url, sync);
			if (document.querySelector('[name="csrf-token"]')) {
				let token = document.querySelector('[name="csrf-token"]').getAttribute('content');
				x.setRequestHeader("X-CSRF-TOKEN", token);
			}
			x.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
			x.onreadystatechange = function() {
				if (x.readyState == 4) {
					callback(x.responseText)
				}
			};
			x.send(data);
		},
    	post: function(url, data, callback, async) {
		    ajax.send(url, callback, 'POST', data, async);
	    }
	};

	let getRequestAnimationFrame = function() {
		return requestAnimFrame;
	};

	let queryParse = function (str) {
		if (typeof str !== 'string') {
			return {};
		}

		str = str.trim().replace(/^(\?|#|&)/, '');

		if (!str) {
			return {};
		}

		return str.split('&').reduce(function (ret, param) {
			let parts = param.replace(/\+/g, ' ').split('=');
			let key = parts[0];
			let val = parts[1];

			key = decodeURIComponent(key);

			// missing `=` should be `null`:
			// http://w3.org/TR/2012/WD-url-20120524/#collect-url-parameters
			val = val === undefined ? null : decodeURIComponent(val);

			if (!ret.hasOwnProperty(key)) {
				ret[key] = val;
			} else if (Array.isArray(ret[key])) {
				ret[key].push(val);
			} else {
				ret[key] = [ret[key], val];
			}

			return ret;
		}, {});
	};

	let getIE = function () {
		let rv = -1,
				ua = navigator.userAgent,
				re;

		if (navigator.appName == 'Microsoft Internet Explorer') {

			re  = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
			if (re.exec(ua) != null) rv = parseFloat( RegExp.$1 );
		}
		else if (navigator.appName == 'Netscape') {
			re  = new RegExp("Trident/.*rv:([0-9]{1,}[\.0-9]{0,})");
			if (re.exec(ua) != null) rv = parseFloat( RegExp.$1 );
		}

		return rv;
	};

	let debounce = function (f, ms) {
		let state = null,
			  COOLDOWN = 1;

		return function() {
			if (state) return;

			f.apply(this, arguments);

			state = COOLDOWN;

			setTimeout(function() { state = null }, ms);
		}
	};

    let delay = function(f, ms) {
        return function () {
            var savedThis = this;
            var savedArgs = arguments;

            setTimeout(function () {
                f.apply(savedThis, savedArgs);
            }, ms);
        };
    };

	let throttle = function (func, ms) {
		let isThrottled = false,
			savedArgs,
			savedThis;

		function wrapper() {

			if (isThrottled) {
				savedArgs = arguments;
				savedThis = this;
				return;
			}

			func.apply(this, arguments);

			isThrottled = true;

			setTimeout(function() {
				isThrottled = false;
				if (savedArgs) {
					wrapper.apply(savedThis, savedArgs);
					savedArgs = savedThis = null;
				}
			}, ms);
		}

		return wrapper;
	};

    let axiosConfig = function () {
    	let token;
        let config = {
            headers: {
                "x-requested-with": "XMLHttpRequest"
            }
        };
        if (document.querySelector('[name="csrf-token"]')) {
            token = document.querySelector('[name="csrf-token"]').getAttribute('content');
            config.headers['X-CSRF-TOKEN'] = token;
        }
        return config
    };


	export default {
		offset: offset,
		Tween: Tween,
		getRequestAnimationFrame: getRequestAnimationFrame,
		ajax: ajax,
		queryParse: queryParse,
		getIE: getIE,
		debounce: debounce,
        delay: delay,
		throttle: throttle,
        axiosConfig: axiosConfig
	}

