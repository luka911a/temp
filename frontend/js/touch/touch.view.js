import dispatcher from 'dispatcher';
import store from 'touch/touch.store';
import fastClick from 'fastClick';

	

	var _handleChange = function() {
		var storeData = store.getData();
		if (storeData.isTouchDevice) {
			fastClick.attach(document.body);
		}
	}

	var init = function() {
		_handleChange();
	}

	export default {
		init: init
	}
