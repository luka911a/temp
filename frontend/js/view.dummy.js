import dispatcher from 'dispatcher';
import store from 'store';

	//!!!replace!
	let idName = 'new-id-';
	let idNum  = 1;


	let _handleChange = function() {
		let storeData = store.getData();
	}

	let _handleMutate = function() {

	}

	let init = function() {
		_handleMutate();
		_handleChange();

		store.eventEmitter.subscribe(_handleChange);

		dispatcher.subscribe(function(e) {
			if (e.type === 'mutate') {
				_handleMutate();
				_handleChange();
			}
		});
	}

	export default {
		init: init
	}
