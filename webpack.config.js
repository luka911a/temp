let webpack = require('webpack');
let path = require('path');
let ExtractTextPlugin = require('extract-text-webpack-plugin');
let autoprefixer = require('autoprefixer');
let CleanWebpackPlugin = require('clean-webpack-plugin');
let UglifyJsPlugin = require('uglifyjs-webpack-plugin');
let HtmlWebpackPlugin = require('html-webpack-plugin');
let cssnano = require('cssnano');
let NODE_ENV = process.env.NODE_ENV;

module.exports = {
    mode: NODE_ENV,
    context: __dirname,
    entry: {
        polyfills: './frontend/js/polyfills.js',
        index: './frontend/js/entry.js'
    },
    output: {
        filename: '[name].app.js',
        path: __dirname + '/public/build/',
        publicPath: '/build/',
        library: '[name]',
    },
    optimization: {
        splitChunks: {
            chunks: "async",
            minSize: 30000,
            minChunks: 1,
            maxAsyncRequests: 5,
            maxInitialRequests: 3,
            automaticNameDelimiter: '~',
            name: true,
            cacheGroups: {
                vendors: {
                    test: /[\\/]node_modules[\\/]/,
                    priority: -10
                },
                default: {
                    minChunks: 2,
                    priority: -20,
                    reuseExistingChunk: true
                }
            }
        }
    },
    resolve: {
        extensions: ['*', '.js', '.jsx'],
        modules: ['./frontend/js', 'node_modules', './frontend/css', './frontend/images', './frontend/glsl']
    },
    module: {
        rules: [
            {
                test: /\.jsx?$|\.js$/,
                exclude: /node_modules/,
                loader: "babel-loader",
                query: {
                    presets: ["@babel/preset-env"]
                }
            },
            {
                test: /\.woff2?$|\.ttf$|\.eot$|\.svg$|\.png|\.jpe?g|\.gif$|\.mp4$|\.webm$/,
                loader: 'file-loader'
            },
            {
                test: /\.(glsl|frag|vert)$/,
                loader: 'raw-loader',
                exclude: /node_modules/
            },
            {
                test: /\.(glsl|frag|vert)$/,
                loader: 'glslify-loader',
                exclude: /node_modules/ }
        ]
    },
    plugins: [
        new ExtractTextPlugin({
            filename: 'style.css',
            disable: false,
            allChunks: true
        }),
        new CleanWebpackPlugin()
    ],
    watch: NODE_ENV == 'development',
    watchOptions: {
        aggregateTimeout: 100
    },
    devtool: NODE_ENV == 'development' ? "eval-source-map" : "",
    devServer: {
        // headers: {
        //     "Access-Control-Allow-Origin": "*",
        //     "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE, PATCH, OPTIONS",
        //     "Access-Control-Allow-Headers": "X-Requested-With, content-type, Authorization"
        // },
        
        before(app) {
            let arr = [
                '/form-json-response.json'
            ];
            for (let i = 0; i < arr.length; i++) {
                app.post(arr[i], function(req, res) {
                    res.redirect(arr[i]);
                });
            }
        }
    }
};

if (NODE_ENV == 'production') {
    module.exports.plugins.push(
        new UglifyJsPlugin({
            uglifyOptions: {
                cache: true,
                compress: true,
                parallel: true,
                output: {
                    comments: false,
                    beautify: false,
                }
            }
        })
    );
    module.exports.module.rules.push(
         {
            test: /\.scss$|\.css$/,
            use: ExtractTextPlugin.extract({
                fallback: 'style-loader',
                use: [
                    {
                        loader: 'css-loader',
                    },
                    {
                        loader: 'postcss-loader',
                        options: {
                            plugins: [
                                cssnano({
                                    preset: ['default', {
                                        discardComments: {
                                            removeAll: true
                                        }
                                    }]
                                })
                            ]
                        }
                    },
                    'sass-loader'
                ],
                publicPath: './'
            })
         }
    );
} else if (NODE_ENV == 'none') {
    module.exports.plugins.push(
        new UglifyJsPlugin({
            uglifyOptions: {
                cache: true,
                compress: false,
                parallel: true,
                sourceMap: true,
                output: {
                    comments: false,
                    beautify: true
                }
            }
        })
    );
    module.exports.module.rules.push(
        {
            test: /\.scss$/,
            use: ExtractTextPlugin.extract({
                fallback: 'style-loader',
                use: [
                    {
                        loader: 'css-loader',
                        options:  {
                            sourceMap: true
                        }
                    },
                    'postcss-loader',
                    'sass-loader'
                ],
                publicPath: './'
            })
        }
    );
} else if (NODE_ENV == 'development') {
    module.exports.module.rules.push(
        {
            test: /\.scss$/,
            use: [
                {
                    loader: 'style-loader'
                },
                {
                    loader: 'css-loader'
                },
                'postcss-loader',
                {
                    loader: 'sass-loader', options: {
                        sourceMap: true
                    }
                }
            ]
        }
    );
}
